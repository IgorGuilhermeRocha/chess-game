package com.chessgame.boardgame;

public class Board {

    private Integer rows;
    private Integer columns;
    private Piece[][] pieces;

    // contructor

    public Board(Integer rows, Integer columns) {
        this.rows = rows;
        this.columns = columns;
        this.pieces = new Piece[rows][columns];
    }

    // getters and seters

    public Piece piece(int row, int column) {
        return pieces[row][column];
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getColumns() {
        return columns;
    }

    public void setColumns(Integer columns) {
        this.columns = columns;
    }

}
