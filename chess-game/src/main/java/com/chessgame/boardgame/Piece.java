package com.chessgame.boardgame;

public class Piece {

    protected Position position;

    private Board board;

    // contructor

    public Piece(Board board) {
        this.position = null;
        this.board = board;
    }

    // getters and setters

    protected Board getBoard() {
        return board;
    }
}
