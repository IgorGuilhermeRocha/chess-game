package com.chessgame;

import com.chessgame.chess.ChessMatch;
import com.chessgame.utils.UI;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {

        ChessMatch chessMatch = new ChessMatch();
        UI.printBoard(chessMatch.getPieces());
    }
}
